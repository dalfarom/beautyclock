<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\proveedor;
use App\Models\tipoUsuario;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tipoUsuario()  {
        return $this->belongsTo(tipoUsuario::class, 'tipo_idtipo', 'idtipo');
    }

    public function proveedor()     {
        return $this->hasOne(proveedor::class, 'users_id', 'id');
    }
}
