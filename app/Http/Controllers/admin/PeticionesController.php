<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\peticiones;

class PeticionesController extends Controller
{
    public function index()
    {
        $peticiones = peticiones::all();

        return view('admin.peticiones.index', array('peticiones' => $peticiones));
    }

    public function cambiarEstado(Request $request)
    {
        $peticion = peticiones::find($request->peticion);

        $peticion->estado = 1;
        $peticion->save();

        return redirect('/admin/peticiones/index');
    }

    public function eliminar(Request $request)
    {
        $peticion = peticiones::find($request->peticion);
        $peticion->delete();

        return redirect('/admin/peticiones/index');
    }

}
