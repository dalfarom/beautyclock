<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ciudad;
use App\Models\pais;

class CiudadController extends Controller
{
    public function index()
    {
        $ciudades = ciudad::all();


        return view('admin.ciudad.index', array('ciudades' => $ciudades));
    }

    public function crear()
    {
        $paises = pais::all();

        return view('admin.ciudad.crear', array('paises' => $paises));
    }

    public function guardar(Request $request)
    {

        $ciudad = new ciudad();
        $ciudad->nombre = $request->nombre;
        $ciudad->pais_idpais = $request->pais;
        $ciudad->save();

        return redirect('/admin/ciudad/index');

    }

    public function eliminar(Request $request)
    {
        $ciudad = ciudad::find($request->ciudad);
        $ciudad->delete();

        return redirect('/admin/ciudad/index');

    }
}
