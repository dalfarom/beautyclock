<?php

namespace App\Http\Controllers\admin;

use App\Models\cupos;
use App\Models\peticiones;
use App\Models\proveedor;
use App\Models\reserva;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $usuarios = User::all()->where('tipo_idtipo', 2)->count();
        $usuario = User::all()->where('tipo_idtipo', 2)->last();

        $clientes = proveedor::all()->count();
        $contacto = peticiones::all()->last();

        $cupos = cupos::all()->count();
        $cupo = cupos::all()->last();

        $reservas = reserva::all()->count();
        $reserva = reserva::all()->last();

        return view('admin.dashboard', array('usuarios' => $usuarios, 'usuario' => $usuario,
                                                    'clientes' => $clientes, 'contacto' => $contacto,
                                                    'cupos' => $cupos, 'cupo' => $cupo,
                                                    'reservas' => $reservas, 'reserva' => $reserva));
    }


}
