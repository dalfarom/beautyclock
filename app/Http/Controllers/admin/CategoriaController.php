<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\categoria;

class CategoriaController extends Controller
{
    public function index()
    {
        $categorias = categoria::all();

        return view('admin.categorias.index', array('categorias' => $categorias));
    }

    public function crear()
    {
        return view('admin.categorias.crear');
    }

    public function guardar(Request $request)
    {
        $categoria = new categoria();
        $categoria->nombre  = $request->nombre;
        $categoria->save();

        return redirect('/admin/categorias/index');
    }

    public function modificar(Request $request)
    {
        $categoria = categoria::find($request->categoria);

        return view('admin.categorias.modificar', array('categoria' => $categoria));
    }

    public function guardarModificaciones(Request $request)
    {
        $categoria = categoria::find($request->categoria);
        $categoria->nombre  = $request->nombre;
        $categoria->save();

        return redirect('/admin/categorias/index');
    }

    public function eliminar(Request $request)
    {
        $categoria = categoria::find($request->categoria);
        $categoria->delete();

        return redirect('/admin/categorias/index');
    }
}
