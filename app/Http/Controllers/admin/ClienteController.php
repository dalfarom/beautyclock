<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\ciudad;
use App\Models\proveedor;

class ClienteController extends Controller
{

    public function index()
    {
        $proveedores = proveedor::all();

        return view('admin.clientes.index', array('proveedores' => $proveedores));
    }

    public function crear()
    {
        $usuarios = User::all()->where('tipo_idtipo', 3);
        $ciudades = ciudad::all();

        return view('admin.clientes.crear', array('usuarios' => $usuarios, 'ciudades' => $ciudades));
    }

    public function guardar(Request $request)
    {
        $proveedor = new proveedor();
        $proveedor->users_id = $request->usuario;
        $proveedor->ciudad_idciudad = $request->ciudad;
        $proveedor->direccion_calle = $request->calle;
        $proveedor->direccion_numero = $request->numero;
        $proveedor->telefono = $request->telefono;
        $proveedor->nombre = $request->nombre;
        $proveedor->save();

        return redirect('/admin/cliente/index');

    }

    public function modificar(Request $request)
    {
        $proveedor = proveedor::find($request->proveedor);
        $ciudades = ciudad::all();

        return view('admin.clientes.modificar', array('proveedor' => $proveedor, 'ciudades' => $ciudades));
    }

    public function guardarModificacion(Request $request)
    {
        $proveedor = proveedor::find($request->proveedor);
        $proveedor->ciudad_idciudad = $request->ciudad;
        $proveedor->direccion_calle = $request->calle;
        $proveedor->direccion_numero = $request->numero;
        $proveedor->telefono = $request->telefono;
        $proveedor->nombre = $request->nombre;
        $proveedor->save();

        return redirect('/admin/cliente/index');
    }

    public function eliminar(Request $request)
    {
        $proveedor = proveedor::find($request->proveedor);
        $proveedor->delete();

        return redirect('/admin/cliente/index');
    }
}
