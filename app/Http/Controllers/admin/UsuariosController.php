<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\tipoUsuario;

class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = User::all();


        return view('admin.usuarios.index', array('usuarios' => $usuarios));
    }

    public function eliminar(Request $request)
    {
        $usuario = User::find($request->usuario);
        $usuario->delete();

        return redirect('/admin/usuarios/index');
    }

    public function modificar(Request $request)
    {
        $usuario = User::find($request->usuario);

        return view('admin.usuarios.modificar', array('usuario' => $usuario));
    }

    public function guardarModificacion(Request $request)
    {
        $usuario = User::find($request->usuario);
        $usuario->primer_nombre = $request->primer_nombre;
        $usuario->apellido_paterno = $request->apellido_paterno;
        $usuario->email = $request->email;
        $usuario->telefono = $request->telefono;

        if ($request->password){
            $password = Hash::make($request->password);
            $usuario->password = $password;
        }

        $usuario->save();
        return redirect('/admin/usuarios/index');
    }

    public function crear()
    {
        $tipos = tipoUsuario::all();
        return view('admin.usuarios.crear', array('tipos' => $tipos));
    }

    public function guardar(Request $request)
    {
        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->primer_nombre = $request->primer_nombre;
        $usuario->apellido_paterno = $request->apellido_paterno;
        $usuario->email = $request->email;
        $usuario->telefono = $request->telefono;
        $usuario->tipo_idtipo = $request->tipo;
        $ahora = now();
        $usuario->created_at = $ahora;
        $usuario->updated_at = $ahora;
        $usuario->password = Hash::make($request->password);
        $usuario->save();

        return redirect('/admin/usuarios/index');
    }
}
