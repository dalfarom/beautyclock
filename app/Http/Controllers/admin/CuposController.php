<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\proveedor;
use App\Models\cupos;

class CuposController extends Controller
{
    public function index()
    {
        $cupos = cupos::all();

        return view('admin.cupos.index', array('cupos' => $cupos));
    }

    public function eliminar(Request $request)
    {
        $cupo = cupos::find($request->cupo);
        $cupo->delete();

        return redirect('/admin/cupos/index');
    }
}
