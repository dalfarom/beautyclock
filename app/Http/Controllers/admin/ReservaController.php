<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\reserva;

class ReservaController extends Controller
{
    public function index()
    {
        $reservas = reserva::all();

        return view('admin.reservas.index', array('reservas' => $reservas));
    }

    public function eliminar(Request $request)
    {
        $reserva = reserva::find($request->reserva);
        $reserva->delete();

        return redirect('/admin/reservas/index');
    }
}
