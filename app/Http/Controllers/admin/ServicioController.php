<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\servicio;
use App\Models\categoria;

class ServicioController extends Controller
{

    public function index()
    {
        $servicios = servicio::all();

        return view('admin.servicios.index', array('servicios' => $servicios));
    }

    public function crear()
    {
        $categorias = categoria::all();

        return view('admin.servicios.crear', array('categorias'=> $categorias));
    }

    public function guardar(Request $request)
    {
        $servicio = new servicio();
        $servicio->nombre  = $request->nombre;
        $servicio->categoria_idcategoria = $request->categoria;
        $servicio->save();

        return redirect('/admin/servicios/index');
    }

    public function modificar(Request $request)
    {
        $servicio = servicio::find($request->servicio);
        $categorias = categoria::all();

        return view('admin.servicios.modificar', array('servicio' => $servicio, 'categorias' => $categorias));
    }

    public function guardarModificaciones(Request $request)
    {
        $servicio = servicio::find($request->servicio);
        $servicio->nombre  = $request->nombre;
        $servicio->categoria_idcategoria = $request->categoria;
        $servicio->save();

        return redirect('/admin/categorias/index');
    }

    public function eliminar(Request $request)
    {
        $servicio = servicio::find($request->servicio);
        $servicio->delete();

        return redirect('/admin/servicios/index');
    }

}
