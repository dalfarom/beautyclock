<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\tipoUsuario;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();

        if($usuario->tipo_idtipo == 1){
            return redirect('/admin/dashboard');
        }elseif ($usuario->tipo_idtipo == 2){
            return 'es usuario';
        }elseif ($usuario->tipo_idtipo == 3){
            return 'es proveedor';
        }else{
            return 'no funciona';
        }

        //return view('home');
    }
}
