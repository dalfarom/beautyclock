<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\pais;

class ciudad extends Model
{
    public $table = 'ciudad';
    public $timestamps = false;
    protected $primaryKey = 'idciudad';


/*
    public function centrocosto()   {
        return $this->belongsTo(\maitena\Models\CentroCosto::class, "idcentrocosto", "idcentrocosto", "centrocosto");
    }

    public function departamento()  {
        return $this->belongsTo(\maitena\Models\Departamento::class, "iddepartamento", "iddepartamento");
    }

    public function area()  {
        return $this->belongsTo(\maitena\Models\Areas::class, "idarea", "idarea");
    }
*/
    public function pais()     {
        return $this->belongsTo(pais::class, "pais_idpais", "idpais");
    }

}
