<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\ciudad;

class proveedor extends Model
{
    public $table = 'proveedor';
    public $timestamps = false;
    protected $primaryKey = 'idproveedor';


    public function user()     {
        return $this->hasOne(User::class, 'id', 'users_id');
    }
    public function ciudad()     {
        return $this->BelongsTo(ciudad::class, 'ciudad_idciudad', 'idciudad');
    }
}
