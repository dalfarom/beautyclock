<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ciudad;

class peticiones extends Model
{
    public $table = 'peticiones';
    public $timestamps = false;
    protected $primaryKey = 'idpeticiones';

    public function ciudad()     {
        return $this->BelongsTo(ciudad::class, 'ciudad_idciudad', 'idciudad');
    }
}
