<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\categoria;

class servicio extends Model
{

    public $table = 'servicio';
    public $timestamps = false;
    protected $primaryKey = 'idservicio';


    public function categoria()
    {
        return $this->belongsTo(categoria::class,'categoria_idcategoria', 'idcategoria');
    }
}
