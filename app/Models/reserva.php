<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\cupos;
use App\Models\servicio;
use App\Models\proveedor;
use App\User;

class reserva extends Model
{
    public $table = 'reserva';
    public $timestamps = false;
    protected $primaryKey = 'idreserva';

    public function cupos()     {
        return $this->BelongsTo(cupos::class, 'cupos_idcupos', 'idcupos');
    }

    public function servicio()
    {
        return $this->BelongsTo(servicio::class, 'servicio_idservicio', 'idservicio');
    }

    public function proveedor()
    {
        return $this->BelongsTo(proveedor::class, 'proveedor_idproveedor', 'idproveedor');
    }

    public function user()
    {
        return $this->BelongsTo(User::class, 'users_id', 'id');
    }

}
