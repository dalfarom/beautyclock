<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ciudad;

class pais extends Model
{
    public $table = 'pais';
    public $timestamps = false;
    protected $primaryKey = 'idpais';


    public function ciudad()     {
        return $this->hasMany(ciudad::class, "pais_idpais", "idpais");
    }
}
