<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    public $table = 'categoria';
    public $timestamps = false;
    protected $primaryKey = 'idcategoria';
}
