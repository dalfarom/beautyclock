<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\proveedor;
use App\Models\ciudad;
use App\Models\servicio;

class cupos extends Model
{
    public $table = 'cupos';
    public $timestamps = false;
    protected $primaryKey = 'idcupos';


    public function proveedor()     {
        return $this->belongsTo(proveedor::class,'proveedor_idproveedor', 'idproveedor');
    }

    public function servicio()
    {
        return $this->belongsTo(servicio::class,'servicio_idservicio', 'idservicio');
    }

}
