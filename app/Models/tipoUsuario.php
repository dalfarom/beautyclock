<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tipoUsuario extends Model
{
    public $table = 'tipo';
    public $timestamps = false;
    protected $primaryKey = 'idtipo';


/*
    public function centrocosto()   {
        return $this->belongsTo(\maitena\Models\CentroCosto::class, "idcentrocosto", "idcentrocosto", "centrocosto");
    }

    public function departamento()  {
        return $this->belongsTo(\maitena\Models\Departamento::class, "iddepartamento", "iddepartamento");
    }

    public function area()  {
        return $this->belongsTo(\maitena\Models\Areas::class, "idarea", "idarea");
    }
*/
    public function user()     {
        return $this->hasMany(\app\Models\User::class, "tipo_idtipo", "idtipo");
    }

}
