@extends('layout.principal')

@section('content')

<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-header">
            <span>Dashboard</span>
        </div>
        <div class="collapse navbar-collapse">

        </div>
    </div>
</nav>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <span>Usuarios</span>
                    </div>
                    <div class="card-content">
                        <div class="col-md-12">
                            <h4>Ultimo usuario registrado: {{ $usuario->primer_nombre }} {{ $usuario->apellido_paterno }}</h4>
                            <h4>Total de usuarios: {{ $usuarios }}</h4>
                            <div class="row">
                                <a class="btn btn-primary" href="/admin/usuarios/index">Ver mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <span>Clientes</span>
                    </div>
                    <div class="card-content">
                        <div class="col-md-12">
                            <h4>Ultimo Contacto: {{ $contacto->nombre }} {{ $contacto->email }}</h4>
                            <h4>Total de clientes: {{ $clientes }}</h4>
                            <div class="row">
                                <a class="btn btn-primary" href="/admin/cliente/index">Ver clientes</a>
                                <a class="btn btn-primary" href="/admin/peticiones/index">Ver peticiones</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <span>Cupos</span>
                    </div>
                    <div class="card-content">
                        <div class="col-md-12">
                            <h4>Ultimo cupo creado: {{ $cupo->proveedor->nombre }}</h4>
                            <h4>Total de cupos creados: {{ $cupos }}</h4>
                            <div class="row">
                                <a class="btn btn-primary" href="/admin/cupos/index">Ver mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <span>Reservas</span>
                    </div>
                    <div class="card-content">
                        <div class="col-md-12">
                            <h4>Ultima reserva realizada: {{ $reserva->user->primer_nombre }} {{ $reserva->user->apellido_paterno }}</h4>
                            <h4>Total de reservas: {{ $reservas }}</h4>
                            <div class="row">
                                <a class="btn btn-primary" href="/admin/reservas/index">Ver mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
