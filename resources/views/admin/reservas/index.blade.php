@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Reservas</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Reservas</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Usuario</strong></th>
                                            <th><strong>Servicio</strong></th>
                                            <th><strong>Proveedor</strong></th>
                                            <th><strong>Cantidad Total</strong></th>
                                            <th><strong>Fecha</strong></th>
                                            <th><strong>Hora</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($reservas as $reserva)
                                            <tr>
                                                <td>{{ $reserva->user->primer_nombre }} {{ $reserva->user->apellido_paterno }}</td>
                                                <td>{{ $reserva->servicio->nombre }}</td>
                                                <td>{{ $reserva->proveedor->nombre }}</td>
                                                <td>{{ $reserva->cupos->cantidad }}</td>
                                                <td>{{ $reserva->cupos->fecha }}</td>
                                                <td>{{ $reserva->cupos->hora_inicio }}</td>
                                                <td>
                                                    <form action="/admin/reservas/eliminar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="reserva" value="{{ $reserva->idreserva }}">
                                                        <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection
