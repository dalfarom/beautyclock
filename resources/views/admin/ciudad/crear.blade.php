@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Ciudades</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Ciudades</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/admin/ciudad/guardar" method="POST">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="ciudad">Ciudad</label>
                                                <input type="text" name="nombre" class="form-control" id="ciudad" placeholder="Ingrese nombre de ciudad" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="pais">Pais</label>
                                                <select class="form-control" id="pais" name="pais">
                                                    <option disabled selected>Seleccione una opcion</option>
                                                    @foreach($paises as $pais)
                                                        <option value="{{ $pais->idpais }}">{{ $pais->nombre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input class="btn btn-primary" type="submit"></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection