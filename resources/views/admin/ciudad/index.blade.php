@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Ciudades</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Ciudades</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <a class="btn btn-primary" href="/admin/ciudad/crear">Nueva Ciudad</a>
                                </div>
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Nombre</strong></th>
                                            <th><strong>Pais</strong></th>
                                            <th><strong>Acción</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                                @foreach($ciudades as $ciudad)
                                                    <tr>
                                                        <td>{{  $ciudad->nombre }}</td>
                                                        <td>{{  $ciudad->pais->nombre  }}</td>
                                                        <td>
                                                            <form action="/admin/ciudad/eliminar" method="post">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="ciudad" value="{{ $ciudad->idciudad }}">
                                                                <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection
