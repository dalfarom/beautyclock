@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Servicios</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Servicios</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/admin/servicios/guardar" method="POST">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="nombre">Servicio</label>
                                                <input type="text" name="nombre" class="form-control" id="nombre"  placeholder="Ingrese Nombre del Servicio" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="tipo">Tipo de Usuario</label>
                                                <select class="form-control" id="usuario" name="categoria" required>
                                                    <option disabled selected>Seleccione una opcion</option>
                                                    @foreach($categorias as $categoria)
                                                        <option value="{{ $categoria->idcategoria }}">{{ $categoria->nombre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input class="btn btn-primary" type="submit"></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection