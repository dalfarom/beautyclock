@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Solicitud de Contacto</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Solicitud de Contacto</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Nombre</strong></th>
                                            <th><strong>Telefono</strong></th>
                                            <th><strong>Email</strong></th>
                                            <th><strong>Fecha</strong></th>
                                            <th><strong>Estado</strong></th>
                                            <th><strong>Ciudad</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($peticiones as $peticion)
                                            <tr>
                                                <td>{{  $peticion->nombre }}</td>
                                                <td>{{  $peticion->telefono }}</td>
                                                <td>{{  $peticion->email }}</td>
                                                <td>{{  $peticion->fecha }}</td>
                                                @if($peticion->estado == 0)
                                                    <td>No Atendido</td>
                                                @else
                                                    <td>Atendido</td>
                                                @endif
                                                <td>{{  $peticion->ciudad->nombre }}</td>
                                                <td>
                                                    <form action="/admin/peticiones/eliminar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="peticion" value="{{ $peticion->idpeticiones }}">
                                                        <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                    </form>
                                                    @if(!$peticion->estado == 1)
                                                        <form action="/admin/peticiones/cambiarEstado" method="post">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="peticion" value="{{ $peticion->idpeticiones }}">
                                                            <button class="btn btn-success btn-sm" type="submit">Cambiar Estado</button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection
