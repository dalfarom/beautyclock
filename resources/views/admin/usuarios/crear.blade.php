@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Usuarios</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Usuarios</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/admin/usuarios/guardar" method="POST">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="primer_nombre">Primer Nombre</label>
                                                <input type="text" name="primer_nombre" class="form-control" id="primer_nombre"  placeholder="Ingrese Primer Nombre" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="apellido_paterno">Apellido Paterno</label>
                                                <input type="text" name="apellido_paterno" class="form-control" id="apellido_paterno"  placeholder="Ingrese Apellido Paterno" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" class="form-control" id="email"  placeholder="Ingrese Email" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="telefono">Telefono</label>
                                                <input type="text" name="telefono" class="form-control" id="telefono"  placeholder="Ingrese Telefono" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Rellene solo si desea cambiar password">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="tipo">Tipo de Usuario</label>
                                                <select class="form-control" id="usuario" name="tipo" required>
                                                    <option disabled selected>Seleccione una opcion</option>
                                                    @foreach($tipos as $tipo)
                                                        <option value="{{ $tipo->idtipo }}">{{ $tipo->nombre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="name">Nombre de Usuario</label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Nombre de Usuario">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input class="btn btn-primary" type="submit"></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection