@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Usuarios</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Usuarios</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <a class="btn btn-primary" href="/admin/usuarios/crear">Nuevo Usuario</a>
                                </div>
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Nombre</strong></th>
                                            <th><strong>Telefono</strong></th>
                                            <th><strong>Email</strong></th>
                                            <th><strong>Tipo</strong></th>
                                            <th><strong>Fecha Ingreso</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($usuarios as $usuario)
                                            <tr>
                                                <td>{{  $usuario->primer_nombre }} {{ $usuario->apellido_paterno }}</td>
                                                <td>{{  $usuario->telefono }}</td>
                                                <td>{{  $usuario->email }}</td>
                                                <td>{{  $usuario->tipoUsuario->nombre }}</td>
                                                <td>{{  $usuario->created_at }}</td>
                                                <td>
                                                    <form action="/admin/usuarios/eliminar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="usuario" value="{{ $usuario->id }}">
                                                        <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                    </form>
                                                    <form action="/admin/usuarios/modificar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="usuario" value="{{ $usuario->id }}">
                                                        <button class="btn btn-warning btn-sm" type="submit">Modificar</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection