@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Usuarios</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Usuarios</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/admin/usuarios/guardarModificacion" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $usuario->id }}" name="usuario">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="primer_nombre">Primer Nombre</label>
                                                <input type="text" name="primer_nombre" class="form-control" id="primer_nombre" value="{{$usuario->primer_nombre}}" placeholder="Ingrese Primer Nombre" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="apellido_paterno">Apellido Paterno</label>
                                                <input type="text" name="apellido_paterno" class="form-control" id="apellido_paterno" value="{{$usuario->apellido_paterno}}" placeholder="Ingrese Apellido Paterno" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" class="form-control" id="email" value="{{$usuario->email}}" placeholder="Ingrese Email" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="telefono">Telefono</label>
                                                <input type="text" name="telefono" class="form-control" id="telefono" value="{{$usuario->telefono}}" placeholder="Ingrese Telefono" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Rellene solo si desea cambiar password">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input class="btn btn-primary" type="submit"></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection