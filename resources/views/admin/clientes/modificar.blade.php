@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Clientes</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Clientes</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <form action="/admin/cliente/guardarModificacion" method="POST">
                                        {{ csrf_field() }}
                                        <input  type="hidden" name="proveedor" value="{{ $proveedor->idproveedor }}">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="nombre">Nombre de Empresa</label>
                                                <input type="text" name="nombre" class="form-control" value="{{ $proveedor->nombre }}" id="nombre" placeholder="Ingrese nombre de ciudad" required>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="telefono">Telefono</label>
                                                <input type="text" name="telefono" value="{{ $proveedor->telefono }}" class="form-control" id="nombre" placeholder="Ingrese numero telefonico" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="ciudad">Ciudad</label>
                                                <select class="form-control" id="ciudad" name="ciudad" required >
                                                    <option disabled selected>Seleccione una opcion</option>
                                                    @foreach($ciudades as $ciudad)
                                                        <option value="{{ $ciudad->idciudad }}">{{ $ciudad->nombre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="calle">Calle</label>
                                                <input type="text" name="calle" value="{{ $proveedor->direccion_calle }}" class="form-control" id="calle" placeholder="Ingrese calle" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="numero">Numero</label>
                                                <input type="text" name="numero" value="{{ $proveedor->direccion_numero }}" class="form-control" id="calle" placeholder="Ingrese numero de domicilio" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input class="btn btn-primary" type="submit"></input>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection