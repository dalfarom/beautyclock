@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Clientes</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Clientes</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <a class="btn btn-primary" href="/admin/cliente/crear">Nuevo Proveedor</a>
                                </div>
                                <div class="row">
                                    <table id="tablaClientes" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Nombre</strong></th>
                                            <th><strong>Telefono</strong></th>
                                            <th><strong>Email</strong></th>
                                            <th><strong>Empresa</strong></th>
                                            <th><strong>Ciudad</strong></th>
                                            <th><strong>Pais</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($proveedores as $proveedor)
                                            <tr>
                                                <td>{{ $proveedor->user->primer_nombre }} {{ $proveedor->user->apellido_paterno }}</td>
                                                <td>{{ $proveedor->telefono }}</td>
                                                <td>{{ $proveedor->user->email }}</td>
                                                <td>{{ $proveedor->nombre }}</td>
                                                <td>{{ $proveedor->ciudad->nombre }}</td>
                                                <td>{{ $proveedor->ciudad->pais->nombre }}</td>
                                                <td>
                                                        <form action="/admin/cliente/eliminar" method="post">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="proveedor" value="{{ $proveedor->idproveedor }}">
                                                            <button class="btn btn-danger btn-xs" type="submit">Eliminar</button>
                                                        </form>
                                                        <form action="/admin/cliente/modificar" method="post">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="proveedor" value="{{ $proveedor->idproveedor }}">
                                                            <button class="btn btn-warning btn-xs" type="submit">Modificar</button>
                                                        </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $('#tablaClientes').DataTable();
        } );
    </script>
@endsection