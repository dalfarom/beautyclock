@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Cupos</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Cupos</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th><strong>Hora</strong></th>
                                            <th><strong>Fecha</strong></th>
                                            <th><strong>Servicio</strong></th>
                                            <th><strong>Proveedor</strong></th>
                                            <th><strong>Cantidad</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cupos as $cupo)
                                            <tr>
                                                <td>{{  $cupo->hora_inicio }}</td>
                                                <td>{{  $cupo->fecha }}</td>
                                                <td>{{  $cupo->servicio->nombre }}</td>
                                                <td>{{  $cupo->proveedor->nombre }}</td>
                                                <td>{{  $cupo->cantidad }}</td>
                                                <td>
                                                    <form action="/admin/cupos/eliminar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="cupo" value="{{ $cupo->idcupos }}">
                                                        <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection
