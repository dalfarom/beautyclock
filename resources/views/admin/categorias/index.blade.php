@extends('layout.principal')

@section('content')

    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <span>Categorias</span>
            </div>
            <div class="collapse navbar-collapse">

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header" data-background-color="blue">
                            <span>Categorias</span>
                        </div>
                        <div class="card-content">
                            <div class="col-md-12">
                                <div class="row">
                                    <a class="btn btn-primary" href="/admin/categorias/crear">Nueva Categoria</a>
                                </div>
                                <div class="row">
                                    <table id="tablaCiudades" class="table col-md-12">
                                        <thead>
                                        <tr>
                                            <th style="width: 80%"><strong>Categoria</strong></th>
                                            <th><strong>Accion</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categorias as $categoria)
                                            <tr>
                                                <td>{{ $categoria->nombre }}</td>
                                                <td>
                                                    <form action="/admin/categorias/eliminar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="categoria" value="{{ $categoria->idcategoria }}">
                                                        <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                                    </form>
                                                    <form action="/admin/categorias/modificar" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="categoria" value="{{ $categoria->idcategoria }}">
                                                        <button class="btn btn-warning btn-sm" type="submit">Modificar</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function () {
            $('#tablaCiudades').DataTable();
        } );
    </script>
@endsection
