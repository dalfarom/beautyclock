<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('img/logo.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Beauty o Clock</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" type="text/css"/>
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Data Table-->
    <link href="{{ asset('DataTables/datatables.css')  }}" rel="stylesheet">
    <script src="{{ asset('DataTables/datatables.js') }}" type="text/javascript"></script>
</head>

<body>
        <div class="wrapper" >
                <div class="sidebar"  data-color="blue" data-image="../assets/img/sidebar-1.jpg" style="background-color: white ;position: fixed;top: 0;bottom: 0;left: 0;display: block;overflow-x: hidden;overflow-y: auto">
                    <!--
                Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                Tip 2: you can also add an image using data-image tag
            -->
                    <div class="logo">
                        <div class="row">
                                <center><img src="{{ asset('img/logo.png') }}" class="data-image" alt="" style="width: 150px"></center>
                        </div>
                        <div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                                <center><input class="btn btn-primary" type="submit" value="Cerrar Sesion"></input</center>
                            </form>
                        </div>
                    </div>
                    <div class="sidebar-wrapper">
                        <ul class="nav">
                            <li>
                                <a href="/admin/dashboard">
                                    <i class="material-icons">dashboard</i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/ciudad/index">
                                    <i class="material-icons">location_city</i>
                                    <p>Ciudades</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/categorias/index">
                                    <i class="material-icons">dashboard</i>
                                    <p>Categorias</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/servicios/index">
                                    <i class="material-icons">dashboard</i>
                                    <p>Servicios</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/cliente/index">
                                    <i class="material-icons">person</i>
                                    <p>Clientes</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/peticiones/index">
                                    <i class="material-icons">help</i>
                                    <p>Solicitudes Pendientes</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/usuarios/index">
                                    <i class="material-icons">face</i>
                                    <p>Usuarios</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/cupos/index">
                                    <i class="material-icons">pie_chart</i>
                                    <p>Cupos</p>
                                </a>
                            </li>
                            <li>
                                <a href="/admin/reservas/index">
                                    <i class="material-icons">help</i>
                                    <p>Reservas</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="main-panel">
                    <!-- Contenido -->

                    @yield('content')

                    <!-- Fin Contenido -->
                    <footer class="footer">
                        <div class="container-fluid">

                            <p class="copyright pull-right">
                                &copy;
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>
                                <a href="#">Beauty Clock</a>, <a href="https://diegoalfarodev.net">Desarrollado por Diego Alfaro Developer</a>
                            </p>
                        </div>
                    </footer>
                </div>
            </div>
</body>
<!--   Core JS Files   -->
<!--<script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>-->
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/material.min.js') }}" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="{{ asset('js/chartist.min.js') }}"></script>
<!--  Dynamic Elements plugin -->
<script src="{{ asset('js/arrive.min.js') }}"></script>
<!--  PerfectScrollbar Library -->
<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('js/bootstrap-notify.js') }}"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{ asset('js/material-dashboard.js') }}"></script>


</html>
