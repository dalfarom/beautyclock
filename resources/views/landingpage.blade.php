<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Beauty o Clock</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="./materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="./materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Logo</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ url('/home') }}">Administración</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Administración</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2">Beauty o Clock!</h1>
        <div class="row center">
          <h5 class="header col s12 light">NECESITAS UN SERVICIO DE BELEZA CON BEAUTY O CLOCK ENCONTRARAS CUALQUIER SERVICIO  EL DIA Y HORA QUE TU REQUIERAS</h5>
        </div>
        <div class="row center">
          <a href="#" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Descargar Android</a>
          <a href="#" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Descargar IOS</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="./materialize/background1.jpg" alt="Unsplashed background img 1"></div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">beenhere</i></h2>
            <h5 class="center">Realiza una Cita</h5>

            <p class="light center">Acceso para personas que deseen realizar una reserva.</p>
            <div class="row center">
                <a href="#" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Reservar</a>
              </div>
          </div>
        </div>

     <!--  <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">group</i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div> -->

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">assignment</i></h2>
            <h5 class="center">Revisa tu Agenda</h5>

            <p class="light center">Acceso para proveedores de servicio.</p>
            <div class="row center">
                <a href="#" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Agenda</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light"> </h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="./materialize/background2.jpg" alt="Unsplashed background img 2"></div>
  </div>

  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>¿ Quieres ofrecer tus Servicios ?</h4>
          <p class="center light">Completa el formulario y te contactaremos.</p>
          <div class="row center">

            <form action="none" method="post" onsubmit="" name="formSolicitudProveedor" class="center">
                    <div class="input-field col s6">
                      <input id="first_name" type="text" class="validate">
                      <label for="first_name">Nombre</label>
                    </div>
                    <div class="input-field col s6">
                      <input type="text" class="validate">
                      <label for="last_name">Apellido</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" class="validate">
                        <label for="last_name">E-mail</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" class="validate">
                        <label for="last_name">Fono</label>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                          <textarea id="textarea1" class="materialize-textarea"></textarea>
                          <label for="textarea1">Descripcion Servicios</label>
                        </div>
                    </div>
                    <div class="row center">
                        <a href="#" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Enviar Solicitud</a>
                    </div>
            </form>

          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">Siempre es buen momento para un relajo</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img style="width: 100%" src="./materialize/background3.jpg" alt="Unsplashed background img 3"></div>
  </div>

  <footer class="page-footer teal">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">

        </div>
        <div class="col l3 s12">

        </div>
        <div class="col l3 s12">

        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <br>
      <a class="brown-text text-lighten-3" href="https://diegoalfarodev.net"><img style="width: 20%" src="./materialize/LogoBlanco.png" alt="" srcset=""></a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="./materialize/js/materialize.js"></script>
  <script src="./materialize/js/init.js"></script>

  </body>
</html>
