<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingpage');
});

Auth::routes(
    Route::get('/admin/dashboard', 'admin\DashboardController@index')->name('dashboard'),

    //Rutas ADM Ciudad
    Route::get('/admin/ciudad/index', 'admin\CiudadController@index')->name('ciudad'),
    Route::get('/admin/ciudad/crear', 'admin\CiudadController@crear')->name('ciudadCrear'),
    Route::post('/admin/ciudad/guardar', 'admin\CiudadController@guardar')->name('ciudadGuardar'),
    Route::post('/admin/ciudad/eliminar', 'admin\CiudadController@eliminar')->name('ciudadEliminar'),
    // Fin rutas Ciudad

    //Rutas ADM Clientes
    Route::get('/admin/cliente/index', 'admin\ClienteController@index')->name('cliente'),
    Route::get('/admin/cliente/crear', 'admin\ClienteController@crear')->name('clienteCrear'),
    Route::post('/admin/cliente/guardar', 'admin\ClienteController@guardar')->name('clienteGuardar'),
    Route::post('/admin/cliente/modificar', 'admin\ClienteController@modificar')->name('clienteModificar'),
    Route::post('/admin/cliente/guardarModificacion', 'admin\ClienteController@guardarModificacion')->name('clienteGuardarModificacion'),
    Route::post('/admin/cliente/eliminar', 'admin\ClienteController@eliminar')->name('clienteEliminar'),
    //Fin Rutas Clientes

    //Inicio Rutas ADM Peticiones
    Route::get('/admin/peticiones/index', 'admin\PeticionesController@index')->name('peticiones'),
    Route::post('/admin/peticiones/cambiarEstado', 'admin\PeticionesController@cambiarEstado')->name('peticionesCambiarEstado'),
    Route::post('/admin/peticiones/eliminar', 'admin\PeticionesController@eliminar')->name('peticionesEliminar'),
    //FIn Rutas Peticiones

    //Inicio Rutas ADM Usuarios
    Route::get('/admin/usuarios/index', 'admin\UsuariosController@index')->name('usuarios'),
    Route::post('/admin/usuarios/eliminar', 'admin\UsuariosController@eliminar')->name('usuariosEliminar'),
    Route::post('/admin/usuarios/modificar', 'admin\UsuariosController@modificar')->name('usuariosModificar'),
    Route::post('/admin/usuarios/guardarModificacion', 'admin\UsuariosController@guardarModificacion')->name('usuariosGuardarModificar'),
    Route::get('/admin/usuarios/crear', 'admin\UsuariosController@crear')->name('usuariosCrear'),
    Route::post('/admin/usuarios/guardar', 'admin\UsuariosController@guardar')->name('usuariosGuardar'),
    //Fin Rutas USuarios

    //Inicio Rutas ADM Cupos
    Route::get('/admin/cupos/index', 'admin\CuposController@index')->name('cupos'),
    Route::post('/admin/cupos/eliminar', 'admin\CuposController@eliminar')->name('cuposEliminar'),
    //Fin Rutas Cupos

    //Inicio Rutas ADM Reservas
    Route::get('/admin/reservas/index', 'admin\ReservaController@index')->name('reservas'),
    Route::post('/admin/reservas/eliminar', 'admin\ReservaController@eliminar')->name('reservasEliminar'),
    //Fin Rutas Reservas

    //Inicio Rutas ADM Categorias
    Route::get('/admin/categorias/index', 'admin\CategoriaController@index')->name('categorias'),
    Route::post('/admin/categorias/eliminar', 'admin\CategoriaController@eliminar')->name('categoriasEliminar'),
    Route::get('/admin/categorias/crear', 'admin\CategoriaController@crear')->name('categoriasCrear'),
    Route::post('/admin/categorias/guardar', 'admin\CategoriaController@guardar')->name('categoriasGuardar'),
    Route::post('/admin/categorias/modificar', 'admin\CategoriaController@modificar')->name('categoriasModificar'),
    Route::post('/admin/categorias/guardarModificaciones', 'admin\CategoriaController@guardarModificaciones')->name('categoriasGuardarModificaciones'),
    //Fin rutas Categorias

    //Inicio Rutas ADM Servicios
    Route::get('/admin/servicios/index', 'admin\ServicioController@index')->name('servicios'),
    Route::post('/admin/servicios/eliminar', 'admin\ServicioController@eliminar')->name('serviciosEliminar'),
    Route::get('/admin/servicios/crear', 'admin\ServicioController@crear')->name('serviciosCrear'),
    Route::post('/admin/servicios/guardar', 'admin\ServicioController@guardar')->name('serviciosGuardar'),
    Route::post('/admin/servicios/modificar', 'admin\ServicioController@modificar')->name('serviciosModificar'),
    Route::post('/admin/servicios/guardarModificaciones', 'admin\ServicioController@guardarModificaciones')->name('serviciosGuardarModificaciones')
    //Fin Rutas Servicios
);

Route::get('/home', 'HomeController@index')->name('home');
